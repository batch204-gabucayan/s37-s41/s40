const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require('../auth.js');

// Route for creating a course
router.post("/", auth.verify,  (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("User is notAdmin!");
	}
});

// Route for retrieving all the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all the ACTIVE courses
router.get("/", (req, res) => {
	courseController.getAllActiveCourses().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving specific course
router.get("/:courseId", (req, res) => {
	// console.log(req.params.courseId);
	// console.log(req.params);

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for Updating a course
router.put("/:courseId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	courseController.updateCourse(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
});

// Route for archiving a course
router.put("/:courseId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	courseController.archiveCourse(req.params, userData).then(resultFromController => res.send(resultFromController));
});


module.exports = router;

/*
	Mini-Activity:
		Limit the course creation to admin only. Refactor the course route/course controller.
*/